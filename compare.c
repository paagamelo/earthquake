#include <stdio.h>
#include <stdlib.h>
#include <math.h>

int main( int argc, char* argv[] ) 
{
	if(argc > 4 || argc < 3) {
        fprintf(stderr, "Usage: %s [filename 1] [filename 2]\n", argv[0]);
        return EXIT_FAILURE;
    }
    
    FILE *file1 = fopen(argv[1], "r");
    FILE *file2 = fopen(argv[2], "r");
    
    float delta = 0.000005;
    
    if(argc == 4) {
		delta = atof(argv[3]);
	}
    
    float Emean1;
    float Emean2;
    int c1;
    int c2;
    int firstoccurrence = -1;
    int line = 0;
    while(fscanf(file1, "%i %f", &c1, &Emean1) == 2 && fscanf(file2, "%i %f", &c2, &Emean2)) {
		if(fabs(Emean1 - Emean2) > delta || (c1 != c2)) {
			/*
			printf("FAILED\n");
			printf("c1: %i c2: %i Emean1: %f Emean2: %f\n", c1, c2, Emean1, Emean2);
			printf("line: %i\n", i);
			*/
			printf("%f\n", fabs(Emean1 - Emean2));
			if(firstoccurrence < 0)
				firstoccurrence = line;
		}
		line++;
	}
	if(firstoccurrence > 0)
		printf("first occurrence: %i\n", firstoccurrence);
	
	return 0;
}
