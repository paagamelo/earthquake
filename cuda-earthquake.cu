/****************************************************************************
 *
 * earthquake.c - Simple 2D earthquake model
 *
 * Copyright (C) 2018 Moreno Marzolla <moreno.marzolla(at)unibo.it>
 * Last updated on 2018-12-29
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * ---------------------------------------------------------------------------
 *
 * Lorenzo Paganelli 0000789954
 * 
 * Versione Cuda del progetto di High Performance Computing
 * 2018/2019, corso di laurea in Ingegneria e Scienze Informatiche,
 * Universita' di Bologna. Per una descrizione del modello si vedano
 * le specifiche sulla pagina del corso:
 *
 * http://moreno.marzolla.name/teaching/HPC/
 *
 * Per compilare:
 *
 * nvcc -Wno-deprecated-gpu-targets cuda-earthquake.cu -o cuda-earthquake
 *
 * (il flag -D_XOPEN_SOURCE=600 e' superfluo perche' viene settato
 * nell'header "hpc.h", ma definirlo tramite la riga di comando fa si'
 * che il programma compili correttamente anche se inavvertitamente
 * non si include "hpc.h", o per errore non lo si include come primo
 * file come necessario).
 *
 * Per eseguire il programma si puo' usare la riga di comando seguente:
 *
 * ./cuda-earthquake 100000 256 > out
 *
 * Il primo parametro indica il numero di timestep, e il secondo la
 * dimensione (lato) del dominio. L'output consiste in coppie di
 * valori numerici (100000 in questo caso) il cui significato e'
 * spiegato nella specifica del progetto.
 *
 ****************************************************************************/
#include "hpc.h"
#include <stdio.h>
#include <stdlib.h>     /* rand() */
#include <assert.h>

/* energia massima */
#define EMAX 4.0f
/* energia da aggiungere ad ogni timestep */
#define EDELTA 1e-4
/* numero di ghost cells su ciascun lato di ciascuna riga e di ciascuna colonna,
 * in breve ogni riga è composta da n + 2*HALO elementi - uno per lato -
 * e lo stesso vale per le colonne, in questo modo si crea un dominio esteso 
 * con due righe e due colonne fantasma (le prime e le ultime due) */
#define HALO 1
/* ampiezza dei cuda block in una dimensione */
#define BLKSIZE1D 1024
/* ampiezza dei cuda block in due dimensioni */
#define BLKSIZE2D 32

/**
 * Restituisce un puntatore all'elemento di coordinate (i,j) del
 * dominio grid con n colonne.
 */
__host__ __device__ static inline float *IDX(float *grid, int i, int j, int n)
{
    return (grid + i*n + j);
}

/**
 * Restituisce un numero reale pseudocasuale con probabilita' uniforme
 * nell'intervallo [a, b], con a < b.
 */
float randab( float a, float b )
{
    return a + (b-a)*(rand() / (float)RAND_MAX);
}

/**
 * Inizializza il dominio grid di dimensioni n*n con valori di energia
 * scelti con probabilità uniforme nell'intervallo [fmin, fmax], con
 * fmin < fmax.
 *
 * NON PARALLELIZZARE QUESTA FUNZIONE: rand() non e' thread-safe,
 * qundi non va usata in blocchi paralleli OpenMP; inoltre la funzione
 * non si "comporta bene" con MPI (i dettagli non sono importanti, ma
 * posso spiegarli a chi e' interessato). Di conseguenza, questa
 * funzione va eseguita dalla CPU, e solo dal master (se si usa MPI).
 */
void setup( float* grid, int ext_n, float fmin, float fmax )
{
    for ( int i=HALO; i<ext_n-HALO; i++ ) {
        for ( int j=HALO; j<ext_n-HALO; j++ ) {
            *IDX(grid, i, j, ext_n) = randab(fmin, fmax);
        }
    }
}

/**
 * Inizializza le ghost cells delle matrici cur e next a zero.
 */
__global__ void setup_ghost(float *cur, float *next, int ext_n) 
{
	const int i = threadIdx.x + blockDim.x * blockIdx.x;
    if(i<ext_n) {
		*IDX(cur, i, 0, ext_n) = 0.0f;
		*IDX(cur, 0, i, ext_n) = 0.0f;
		*IDX(cur, ext_n-1, i, ext_n) = 0.0f;
		*IDX(cur, i, ext_n-1, ext_n) = 0.0f;
		*IDX(next, i, 0, ext_n) = 0.0f;
		*IDX(next, 0, i, ext_n) = 0.0f;
		*IDX(next, ext_n-1, i, ext_n) = 0.0f;
		*IDX(next, i, ext_n-1, ext_n) = 0.0f;
	}
}

/**
 * Somma delta a tutte le celle del dominio grid di dimensioni
 * n*n. Questa funzione realizza il passo 1 descritto nella specifica
 * del progetto.
 */
__global__ void increment_energy(float *grid, int ext_n, float delta)
{
    const int i = HALO + threadIdx.x + blockDim.x * blockIdx.x;
    const int j = HALO + threadIdx.y + blockDim.y * blockIdx.y;
    if(i < ext_n-HALO && j < ext_n-HALO) {
		*IDX(grid, i, j, ext_n) += delta;
    }
}

/**
 * Conta il numero di celle la cui energia e' strettamente maggiore di EMAX.
 * Ciascun cuda block inserisce il valore calcolato nell'array di 
 * risultati parziali cs, la CPU effettua la riduzione.
 */
__global__ void count_cells(float *grid, int ext_n, int *cs)
{
	__shared__ int tmp[BLKSIZE2D*BLKSIZE2D];
    /* indice locale per accedere all'array 1D tmp */
    const int lindex = threadIdx.y * blockDim.x + threadIdx.x;
    /* indici globali per accedere al dominio grid */
    const int gi = HALO + threadIdx.x + blockDim.x * blockIdx.x;
    const int gj = HALO + threadIdx.y + blockDim.y * blockIdx.y;
    int bsize = (blockDim.x * blockDim.y) / 2;
    
    tmp[lindex] = (gi < ext_n-HALO && 
				   gj < ext_n-HALO && 
				   *IDX(grid, gi, gj, ext_n) > EMAX) ? 1 : 0;
    __syncthreads();
    /* riduzione con struttura ad albero */
	while(bsize > 0) {
		if(lindex < bsize) {
			tmp[lindex] += tmp[lindex + bsize];
		}
		bsize = bsize / 2;
		__syncthreads();
    }
    if(lindex == 0) {
		int blocksPerRow = ((ext_n - 2*HALO)+BLKSIZE2D-1)/BLKSIZE2D;
		cs[blockIdx.y * blocksPerRow + blockIdx.x] = tmp[0];
	}
}

/** 
 * Distribuisce l'energia di ogni cella a quelle adiacenti (se
 * presenti). cur denota il dominio corrente, next denota il dominio
 * che conterra' il nuovo valore delle energie. Questa funzione
 * realizza il passo 2 descritto nella specifica del progetto.
 */
__global__ void propagate_energy( float *cur, float *next, int ext_n )
{
	__shared__ float buf[BLKSIZE2D][BLKSIZE2D];
    const float FDELTA = EMAX/4;
    const int gi = threadIdx.y + blockIdx.y * (blockDim.y-2);
    const int gj = threadIdx.x + blockIdx.x * (blockDim.x-2);
    const int li = threadIdx.y;
    const int lj = threadIdx.x;
    
    if(gi < ext_n && gj < ext_n) {
		buf[li][lj] = *IDX(cur, gi, gj, ext_n);
		__syncthreads();
		
		if(li>0 && (li<blockDim.y-1) &&
		   lj>0 && (lj<blockDim.x-1) &&
		   (gi<ext_n-HALO) && (gj<ext_n-HALO)) 
		{
			float F = buf[li][lj];
			
			int n = ((buf[li][lj-1] > EMAX) ? 1 : 0) +
					((buf[li][lj+1] > EMAX) ? 1 : 0) +
					((buf[li-1][lj] > EMAX) ? 1 : 0) +
					((buf[li+1][lj] > EMAX) ? 1 : 0);
		
			F += n * FDELTA;
			if (F > EMAX) {
				F -= EMAX;
			}
			
			*IDX(next, gi, gj, ext_n) = F;
		}
    }
}

/**
 * Calcola l'energia media delle celle del dominio grid di
 * dimensioni n*n. Il dominio non viene modificato.
 * Ciascun cuda block inserisce il valore calcolato nell'array di 
 * risultati parziali Emeans, la CPU effettua la riduzione.
 */
__global__ void average_energy(float *grid, int ext_n, float *Emeans)
{
	__shared__ float tmp[BLKSIZE2D*BLKSIZE2D];
    /* indice locale per accedere all'array 1D tmp */
    const int lindex = threadIdx.y * blockDim.x + threadIdx.x;
    /* indici globali per accedere al dominio grid */
    const int gi = HALO + threadIdx.x + blockDim.x * blockIdx.x;
    const int gj = HALO + threadIdx.y + blockDim.y * blockIdx.y;
    int bsize = (blockDim.x * blockDim.y) / 2;
    
    tmp[lindex] = (gi < ext_n-HALO && 
				   gj < ext_n-HALO) ? 
				   *IDX(grid, gi, gj, ext_n) : 0;
    __syncthreads();
    /* riduzione con struttura ad albero */
    while(bsize > 0) {
		if(lindex < bsize) {
			tmp[lindex] += tmp[lindex + bsize];
		}
		bsize = bsize / 2;
		__syncthreads();
    }
    if(lindex == 0) {
		int blocksPerRow = ((ext_n - 2*HALO)+BLKSIZE2D-1)/BLKSIZE2D;
		Emeans[blockIdx.y * blocksPerRow + blockIdx.x] = tmp[0];
	}
}

int main( int argc, char* argv[] )
{
    float *cur, *d_cur, *d_next;
    int s, n = 256, nsteps = 2048;
    float Emean = 0.0f, 
		  *Emeans, //array di risultati parziali dell'host
		  *d_Emeans; //array di risultati parziali del device
    int c = 0, 
        *cs, //array di risultati parziali dell'host
        *d_cs; //array di risultati parziali del device

    srand(19); /* Inizializzazione del generatore pseudocasuale */
    
    if ( argc > 3 ) {
        fprintf(stderr, "Usage: %s [nsteps [n]]\n", argv[0]);
        return EXIT_FAILURE;
    }

    if ( argc > 1 ) {
        nsteps = atoi(argv[1]);
    }

    if ( argc > 2 ) {
        n = atoi(argv[2]);
    }

	/* Dimensione del dominio esteso */
	const int ext_n = n + 2*HALO;
	const size_t size = ext_n * ext_n * sizeof(float);
	/* Numero di blocchi in due dimensioni che verranno lanciati,
	 * corrisponde al numero di elementi che gli array di risultati parziali
	 * dovranno avere */
    const int NBLOCKS = ((n+BLKSIZE2D-1)/BLKSIZE2D) * ((n+BLKSIZE2D-1)/BLKSIZE2D);
    
    /* Allochiamo i domini */
    cur = (float *)malloc(size); assert(cur);
    Emeans = (float *)malloc(NBLOCKS * sizeof(*Emeans)); assert(Emeans);
    cs = (int *)malloc(NBLOCKS * sizeof(*cs)); assert(cs);
    
    cudaMalloc((void **)&d_cur, size);
    cudaMalloc((void **)&d_next, size);
    cudaMalloc((void **)&d_Emeans, NBLOCKS * sizeof(*d_Emeans));
    cudaMalloc((void **)&d_cs, NBLOCKS * sizeof(*d_cs));
    
    /* L'energia iniziale di ciascuna cella e' scelta 
       con probabilita' uniforme nell'intervallo [0, EMAX*0.1] */       
    setup(cur, ext_n, 0, EMAX*0.1);
    /* L'energia delle ghost cells viene settata a zero, 
	 * non sarà più cambiata.
     * Per farlo vengono lanciati cuda blocks in una dimensione, ciascun thread
     * inizializza 4 elementi (uno su ciascuna riga e su ciascuna colonna fantasma) 
     * per ogni matrice, per un totale di 8 elementi */
    setup_ghost<<<(ext_n+BLKSIZE1D-1)/BLKSIZE1D, BLKSIZE1D>>>(d_cur, d_next, ext_n);
    
    /* copia del dominio da host a device */
    cudaMemcpy(d_cur, cur, size, cudaMemcpyHostToDevice);
    
    /* 2d */
    dim3 block(BLKSIZE2D, BLKSIZE2D);
    dim3 grid((n+BLKSIZE2D-1)/BLKSIZE2D, (n+BLKSIZE2D-1)/BLKSIZE2D);
    dim3 grid_shared((n+BLKSIZE2D-3)/(BLKSIZE2D-2), (n+BLKSIZE2D-3)/(BLKSIZE2D-2));
    
    const double tstart = hpc_gettime();
    for (s=0; s<nsteps; s++) {
        
        increment_energy<<<grid, block>>>(d_cur, ext_n, EDELTA);
        cudaDeviceSynchronize();
        
		count_cells<<<grid, block>>>(d_cur, ext_n, d_cs);
        cudaMemcpy(cs, d_cs, NBLOCKS * sizeof(*cs), cudaMemcpyDeviceToHost); // <- barriera implicita
        c = 0;
        for(int i=0; i<NBLOCKS; i++) {
			c += cs[i];
		}
		
		propagate_energy<<<grid_shared, block>>>(d_cur, d_next, ext_n);
        cudaDeviceSynchronize();
        
        average_energy<<<grid, block>>>(d_next, ext_n, d_Emeans);
        cudaMemcpy(Emeans, d_Emeans, NBLOCKS * sizeof(*Emeans), cudaMemcpyDeviceToHost); // <- barriera implicita
		Emean = 0.0f;
		for(int i=0; i<NBLOCKS; i++) {
			Emean += Emeans[i];
		}
		Emean /= (n*n);
		
		printf("%d %f\n", c, Emean);

        float *tmp = d_cur;
        d_cur = d_next;
        d_next = tmp;
    }
    const double elapsed = hpc_gettime() - tstart;
    
    double Mupdates = (((double)n)*n/1.0e6)*nsteps; /* milioni di celle aggiornate per ogni secondo di wall clock time */
    fprintf(stderr, "%s : %.4f Mupdates in %.4f seconds (%f Mupd/sec)\n", argv[0], Mupdates, elapsed, Mupdates/elapsed);

    /* Libera la memoria */
    free(cur);
    free(cs);
    free(Emeans);
    
    cudaFree(d_cur);
    cudaFree(d_next);
    cudaFree(d_cs);
    cudaFree(d_Emeans);
    
    return EXIT_SUCCESS;
}
