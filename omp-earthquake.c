/****************************************************************************
 *
 * earthquake.c - Simple 2D earthquake model
 *
 * Copyright (C) 2018 Moreno Marzolla <moreno.marzolla(at)unibo.it>
 * Last updated on 2018-12-29
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * ---------------------------------------------------------------------------
 *
 * Lorenzo Paganelli 0000789954
 * 
 * Versione Open MP del progetto di High Performance Computing
 * 2018/2019, corso di laurea in Ingegneria e Scienze Informatiche,
 * Universita' di Bologna. Per una descrizione del modello si vedano
 * le specifiche sulla pagina del corso:
 *
 * http://moreno.marzolla.name/teaching/HPC/
 *
 * Per compilare:
 *
 * gcc -std=c99 -Wall -Wpedantic -O2 -D_XOPEN_SOURCE=600 -fopenmp    omp-earthquake.c  -lgomp -o omp-earthquake
 *
 * (il flag -D_XOPEN_SOURCE=600 e' superfluo perche' viene settato
 * nell'header "hpc.h", ma definirlo tramite la riga di comando fa si'
 * che il programma compili correttamente anche se inavvertitamente
 * non si include "hpc.h", o per errore non lo si include come primo
 * file come necessario).
 *
 * Per eseguire il programma si puo' usare la riga di comando seguente:
 *
 * ./omp-earthquake 100000 256 > out
 *
 * Il primo parametro indica il numero di timestep, e il secondo la
 * dimensione (lato) del dominio. L'output consiste in coppie di
 * valori numerici (100000 in questo caso) il cui significato e'
 * spiegato nella specifica del progetto.
 *
 ****************************************************************************/
#include "hpc.h"
#include <stdio.h>
#include <stdlib.h>     /* rand() */
#include <assert.h>

/* energia massima */
#define EMAX 4.0f
/* energia da aggiungere ad ogni timestep */
#define EDELTA 1e-4
/* numero di ghost cells su ciascun lato di ciascuna riga e di ciascuna colonna,
 * in breve ogni riga è composta da n + 2*HALO elementi - uno per lato -
 * e lo stesso vale per le colonne, in questo modo si crea un dominio esteso 
 * con due righe e due colonne fantasma (le prime e le ultime due) */
#define HALO 1

/**
 * Restituisce un puntatore all'elemento di coordinate (i,j) del
 * dominio grid con n colonne.
 */
static inline float *IDX(float *grid, int i, int j, int n)
{
    return (grid + i*n + j);
}

/**
 * Restituisce un numero reale pseudocasuale con probabilita' uniforme
 * nell'intervallo [a, b], con a < b.
 */
float randab( float a, float b )
{
    return a + (b-a)*(rand() / (float)RAND_MAX);
}

/**
 * Inizializza il dominio grid di dimensioni n*n con valori di energia
 * scelti con probabilità uniforme nell'intervallo [fmin, fmax], con
 * fmin < fmax.
 *
 * NON PARALLELIZZARE QUESTA FUNZIONE: rand() non e' thread-safe,
 * qundi non va usata in blocchi paralleli OpenMP; inoltre la funzione
 * non si "comporta bene" con MPI (i dettagli non sono importanti, ma
 * posso spiegarli a chi e' interessato). Di conseguenza, questa
 * funzione va eseguita dalla CPU, e solo dal master (se si usa MPI).
 */
void setup( float* grid, int ext_n, float fmin, float fmax )
{
    for ( int i=HALO; i<ext_n-HALO; i++ ) {
        for ( int j=HALO; j<ext_n-HALO; j++ ) {
            *IDX(grid, i, j, ext_n) = randab(fmin, fmax);
        }
    }
}

/**
 * Somma delta a tutte le celle del dominio grid di dimensioni
 * n*n. Questa funzione realizza il passo 1 descritto nella specifica
 * del progetto.
 */
void increment_energy( float *grid, int ext_n, float delta )
{
	#pragma omp for
    for (int i=HALO; i<ext_n-HALO; i++) {
        for (int j=HALO; j<ext_n-HALO; j++) {
            *IDX(grid, i, j, ext_n) += delta;
        }
    }
}

/**
 * Distribuisce l'energia di ogni cella a quelle adiacenti (se
 * presenti) e calcola le statistiche del caso (n. di celle con 
 * energia > EMAX, energia media). cur denota il dominio corrente, 
 * next denota il dominio che conterra' il nuovo valore delle energie. 
 * Questa funzione realizza il passo 2 descritto nella specifica del progetto.
 * 
 * L'ordine con cui vengono effettuate la propagazione dell'energia e
 * il calcolo delle statistiche è importante:
 * (1) conto delle celle con energia > EMAX
 * (2) propagazione dell'energia
 * (3) calcolo dell'energia media
 */
void step(float *cur, float *next, int ext_n, int *c, float *Emean) 
{
	const float FDELTA = EMAX/4;
	int local_c = 0;
	float local_sum = 0.0f;
	#pragma omp for
    for (int i=HALO; i<ext_n-HALO; i++) {
        for (int j=HALO; j<ext_n-HALO; j++) {
			/* il valore di ciascuna cella viene letto una volta sola */
			float F = *IDX(cur, i, j, ext_n);
			
            /* 
             * (1) conto delle celle con energia > EMAX.
             */
			if ( F > EMAX ) { local_c++; }
			
			/* 
			 * (2) propagazione dell'energia.
			 */
            if (*IDX(cur, i, j-1, ext_n) > EMAX) { F += FDELTA; }
            if (*IDX(cur, i, j+1, ext_n) > EMAX) { F += FDELTA; }
            if (*IDX(cur, i-1, j, ext_n) > EMAX) { F += FDELTA; }
            if (*IDX(cur, i+1, j, ext_n) > EMAX) { F += FDELTA; }

            if (F > EMAX) {
                F -= EMAX;
            }
            /* Si noti che il valore di F potrebbe essere ancora
               maggiore di EMAX; questo non e' un problema:
               l'eventuale eccesso verra' rilasciato al termine delle
               successive iterazioni fino a riportare il valore
               dell'energia sotto la foglia EMAX. */
            *IDX(next, i, j, ext_n) = F;
            
            /* 
             * (3) calcolo dell'energia media.
             * Si noti che non è necessario accedere alla matrice next.
             */
            local_sum += F;
        }
    }
    #pragma omp atomic
    *c += local_c;
    #pragma omp atomic
    *Emean += local_sum;
}

int main( int argc, char* argv[] )
{
    float *cur, *next;
    int s, n = 256, nsteps = 2048;
    float Emean = 0.0f;
    int c = 0;

    srand(19); /* Inizializzazione del generatore pseudocasuale */
    
    if ( argc > 3 ) {
        fprintf(stderr, "Usage: %s [nsteps [n]]\n", argv[0]);
        return EXIT_FAILURE;
    }

    if ( argc > 1 ) {
        nsteps = atoi(argv[1]);
    }

    if ( argc > 2 ) {
        n = atoi(argv[2]);
    }

	/* Dimensione del dominio esteso */
	const int ext_n = n + 2*HALO;
    const size_t size = ext_n * ext_n * sizeof(float);
	
    /* Allochiamo i domini */
    cur = (float*)malloc(size); assert(cur);
    next = (float*)malloc(size); assert(next);

    /* L'energia iniziale di ciascuna cella - escluse le ghost cells - 
     * e' scelta con probabilita' uniforme nell'intervallo [0, EMAX*0.1] */       
    setup(cur, ext_n, 0, EMAX*0.1);
   
	/* L'energia delle ghost cells viene settata a zero, 
	 * non sarà più cambiata */
	#pragma omp parallel for default(none) shared(cur, next)
	for(int i=0; i<ext_n; i++) {
		*IDX(cur, i, 0, ext_n) = 0.0f;
		*IDX(cur, 0, i, ext_n) = 0.0f;
		*IDX(cur, ext_n-1, i, ext_n) = 0.0f;
		*IDX(cur, i, ext_n-1, ext_n) = 0.0f;
		*IDX(next, i, 0, ext_n) = 0.0f;
		*IDX(next, 0, i, ext_n) = 0.0f;
		*IDX(next, ext_n-1, i, ext_n) = 0.0f;
		*IDX(next, i, ext_n-1, ext_n) = 0.0f;
	}
    
	const double tstart = hpc_gettime();
   
    #pragma omp parallel default(none) shared(nsteps, cur, next, n, c, Emean) private(s)
    for (s=0; s<nsteps; s++) {
        
        increment_energy(cur, ext_n, EDELTA);
        #pragma omp barrier
        
        step(cur, next, ext_n, &c, &Emean);
		#pragma omp barrier
		
		#pragma omp single 
		{
			Emean /= (n*n);
			printf("%d %f\n", c, Emean);

			float *tmp = cur;
			cur = next;
			next = tmp;
        
			Emean = 0.0f;
			c = 0;
		} // <- barriera implicita
    }
    const double elapsed = hpc_gettime() - tstart;
    
    double Mupdates = (((double)n)*n/1.0e6)*nsteps; /* milioni di celle aggiornate per ogni secondo di wall clock time */
    fprintf(stderr, "%s : %.4f Mupdates in %.4f seconds (%f Mupd/sec)\n", argv[0], Mupdates, elapsed, Mupdates/elapsed);

    /* Libera la memoria */
    free(cur);
    free(next);
    
    return EXIT_SUCCESS;
}
